<!--<div class="container">

    <div class="jumbotron">
        <div class="row">
            <div class="col-md-4">
            <h2>Titel1</h2>
            <p>Lorem ipsum ipsum ipsum ipsum</p>
            </div>
            <div class="col-md-4">
                <h2>Titel3</h2>
                <p>Lorem ipsum ipsum ipsum ipsum</p>
            </div>
            <div class="col-md-4">
                <h2>Titel2</h2>
                <p>Lorem ipsum ipsum ipsum ipsum</p>
            </div>
            <div class="col-md-4">
                <h2>Titel1</h2>
                <p>Lorem ipsum ipsum ipsum ipsum</p>
            </div>
            <div class="col-md-4">
                <h2>Titel3</h2>
                <p>Lorem ipsum ipsum ipsum ipsum ipsum</p>
            </div>
            <div class="col-md-4">
                <h2>Titel2</h2>
                <p>Lorem ipsum ipsum ipsum ipsum</p>
            </div>
        </div>
    </div>


</div> -->
<div id="footer">

        <div class="col-sm-10">

            <p class="col-sm-4">Gemaakt en ontworppen door: <span class="footerAccent">Robby Van den Broecke</span></p>
            <p class="col-sm-4">In opdracht van: <span class="footerAccent"> &copy;Arteveldehogeschool</span></p>
            <p class="col-sm-4"> Jaar: <span class="footerAccent">2013-2014</p>
        </div>
        <div class="col-sm-2">
            <div class="col-sm-4">
               <a href="https://www.facebook.com/robby.vandenbroecke"> <img class="socialmedia" src="<?php bloginfo('stylesheet_directory');?>/styles/images/facebook.png"></a>
            </div>
            <div class="col-sm-4">
                <a href="https://twitter.com/VdB_Robby"> <img class="socialmedia" src="<?php bloginfo('stylesheet_directory');?>/styles/images/twitter.png"></a>
            </div>

            <div class="col-sm-4">
                <a href="https://bitbucket.org/robbvand3"><img class="socialmedia" src="<?php bloginfo('stylesheet_directory');?>/styles/images/bitbucket.png"></a>
            </div>
        </div>

</div>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

<!-- <script type="text/javascript" src="http://localhost:8080/wordpress/wp-content/themes/portfolio/js/jasny-bootstrap.min.js"></script> -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jasny-bootstrap.min.js"></script>


</body></html>