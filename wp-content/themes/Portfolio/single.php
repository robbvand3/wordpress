<?php get_header(); ?>
<?php get_comments(); ?>
<div class="container">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="entry <?php if(is_home() && $post==$posts[0] && !is_paged()) echo ' firstpost';?>">
        <h3 class="entrytitle" id="post-<?php the_ID(); ?>"> <a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
        <p class="blogdate"> <?php the_time('F jS') ?>, <?php the_time('Y') ?></p>
        <div class="entrybody">

            <?php the_content(__('Read more'));?>


            <p class="blogtags"><?php the_tags(); ?> </p>


        </div>

        <?php trackback_rdf(); ?>
    </div>

    <?php
    $id = get_the_ID();
    //Gather comments for a specific page/post
    $comments = get_comments(array(
        'post_id' => $id,
        'status' => 'approve' //Change this to the type of comments to be displayed
    ));

    //Display the list of comments
    wp_list_comments(array(
        'per_page' => 10, //Allow comment pagination
        'reverse_top_level' => false //Show the latest comments at the top of the list
    ), $comments);
    ?>

<?php endwhile; else: ?>
    <p> <?php _e('Sorry, no posts matched your criteria.'); ?> </p>
<?php endif; ?>

    <?php
    $comments_args = array(
        // change the title of send button
        'label_submit'=>'Send',
        // change the title of the reply section
        'title_reply'=>'Write a Reply or Comment',
        // remove "Text or HTML to be displayed after the set of comment fields"
        'comment_notes_after' => '',
        // redefine your own textarea (the comment body)
        'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><br /><textarea id="comment" name="comment" aria-required="true"></textarea></p>',
    );

    comment_form($comments_args);
    ?>

    <ul class="navigationarrows">
        <li ><?php previous_post_link(' Previous: &laquo; %link'); ?> <?php if(!get_adjacent_post(false, '', true)) { echo '<span>&laquo;Previous:</span>'; } // if there are no older articles ?></li>
        <li ><?php next_post_link('Next:  %link &raquo;'); ?> <?php if(!get_adjacent_post(false, '', false)) { echo '<span>Next:</span>'; } // if there are no newer articles ?> </li>
    </ul>

    <!-- End WordPress Loop -->
</div>

    <!-- end middle content area -->

<?php get_footer(); ?>