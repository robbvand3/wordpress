////NAMELESS FUNCTION - AUTO EXECUTED WHEN DOCUMENT IS LOADED
(function(){
    //alert("Mega template  in HTML5 & CSS3");//ALERT BOX IN WINDOW
    console.log("Mega template  in HTML5 & CSS3");//LOG IN CONSOLE TAB DEVTOOLS

    $('.scrolltotop').click(function(ev){
        ev.preventDefault();
        $('html, body').animate({scrollTop: 0}, 600);
        return false;
    });

    $(window).scroll(function(ev){
        if($(this).scrollTop() > 100){
            $('.scrolltotop').fadeIn();
        }
        else{
            $('.scrolltotop').fadeOut();
        }
    });

    $('.menu-toggle').click(function(ev){
        ev.preventDefault();
        $('#access ul').slideToggle(600, 'linear');
        return false;
    });
})();