<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>Robby Van den Broecke</title>
    <link rel="shortcut icon " href="http://localhost:8080/wordpress/wp-content/themes/Portfolio/styles/images/icon.ico">

    <!-- Bootstrap core CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jasny-bootstrap.min.css" />
    <!-- <link href="http://localhost:8080/wordpress/wp-content/themes/portfolio/css/jasny-bootstrap.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>"

    <!-- Just for debugging purposes. Don t actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css"></style></head>

<body >
<div class="image-header img-polaroid">
    <h1 class="titel">Robby Van den Broecke</h1>
    <h3>- Portfolio -</h3>
</div>


    <!-- Static navbar -->
    <div class="navbar navbar-fixed-top navbar-default" style="">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navbar-offcanvas" data-canvas="body">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Portfolio</a>
        </div>

        <div class="navbar-offcanvas offcanvas" style="">
            <a class="navmenu-brand" href="#">Project name</a>

            <?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'menu_class' => 'nav navbar-nav' ) ); ?>
            <?php wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'menu_class' => 'nav navbar-nav navbar-right' ) ); ?>


            <!--<ul class="nav navbar-nav">
                <li><a href="../navmenu/">Slide in</a></li>
                <li><a href="../navmenu-push/">Push</a></li>
                <li><a href="../navmenu-reveal">Reveal</a></li>
                <li class="active"><a href="./">Off canvas navbar</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li class="dropdown-header">Nav header</li>
                        <li><a href="#">Separated link</a></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
           </ul> -->
        </div><!--/.nav-collapse -->
    </div>